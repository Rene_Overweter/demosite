<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $editorRole = Role::where('name', 'editor')->first();
        $authorRole = Role::where('name', 'author')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Admin user',
            'email' => 'Admin@admin.localhost',
            'password' => Hash::make('supergeheimpassword')
        ]);

        $editor = User::create([
            'name' => 'Editor user',
            'email' => 'Editor@editor.localhost',
            'password' => Hash::make('supergeheimpassword')
        ]);

        $author = User::create([
            'name' => 'Author user',
            'email' => 'Author@author.localhost',
            'password' => Hash::make('supergeheimpassword')
        ]);

        $user = User::create([
            'name' => 'User user',
            'email' => 'User@user.localhost',
            'password' => Hash::make('supergeheimpassword')
        ]);

        $admin->roles()->attach($adminRole);
        $editor->roles()->attach($editorRole);
        $author->roles()->attach($authorRole);
        $user->roles()->attach($userRole);
    }

}
