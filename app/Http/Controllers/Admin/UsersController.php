<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Gate;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // Note : gebruik de auth middleware zo dat men die niet ingelogt zijn op de view van deze controller komen
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
		return view('admin.users.index')->with('users', $users);
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // Note : Gebruik de gate om de ingelogde gebruiker na te kijken of deze een admin rol heeft
        // als die geen admin is dan deze te redirected naar de index pagina zie AuthServiceProvider

        if(Gate::denies('edit-users')){
            return redirect(route('admin.users.index'));
        }

        // Note : Pak de waarde van User en de Roles en geef ze door aan de view
        $roles = Role::all();
        return view('admin.users.edit')->with([
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Note : sync gebruiken om dat in de request een roles array word gestuurd in plaats van een enkele waarde en word dan aan de user relatie gekoppeld
        $user->roles()->sync($request->roles);

        $user->name = $request->name;
        $user->email = $request->email;
        if($user->save()){
            $request->session()->flash('success', $user->name . ' Has been updated.');
        }else{
            $request->session()->flash('error', 'Oops, There was a Error while updating.');
        }

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
