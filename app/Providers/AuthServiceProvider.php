<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Note : maak een gate voor de admin en/of author rol zo dat deze gebruiker de User Management pagina mag gebruiken
        Gate::define('manage-users', function ($user){
            return $user->hasAnyRoles(['admin', 'author']);
        });

        Gate::define('edit-users', function ($user){
            return $user->hasAnyRoles(['admin', 'author']);
        });

        Gate::define('delete-users', function ($user){
            return $user->hasRole('admin');
        });
    }
}
